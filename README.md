# voicemail transcribe

## Overview

1. Twilio のボイスメールの URL を DB に保存
2. ボイスメールの .wav ファイルを Google Cloud Storage にアップロード
3. アップロードした .wav の URL を Cloud Speech API に送る
4. 文字起こしの結果を .txt ファイルとして Google Cloud Storage に保存
5. 文字起こしの内容と音声ファイル、.txt ファイルの URL を Slack に投稿
6. 10 分ごとに実行


## ToDo

- Cloud Storage に保存した .txt ファイルの内容に対して正規表現を用いた置換処理をかける
  - prh と同じように .yaml の設定ファイルに置換パターンを記述する
