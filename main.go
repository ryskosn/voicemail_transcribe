package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/url"
	"os"
	"path"
	"strings"

	"github.com/nlopes/slack"
	twilio "github.com/saintpete/twilio-go"
)

const (
	// Bucket is bucket name at GCS
	Bucket = "voicemail-193213"
)

func fromArg() string {
	log.SetFlags(log.Lshortfile)
	if len(os.Args) < 2 {
		fmt.Fprintln(os.Stderr, "Twilio wav file URL is needed.")
		os.Exit(2)
	}
	return os.Args[1]
}

func transcribe(r Rec) {
	u, err := url.Parse(r.wavURL)
	if err != nil {
		log.Println("Failed to parse Twilio recording URL:", err)
		return
	}
	audioURL := twilioWavURL(u)

	if err := uploadTwilioWav(audioURL); err != nil {
		log.Fatalf("Failed to upload Twilio .wav file: %v", err)
	}
	log.Println(".wav uploaded:", audioURL)

	base := path.Base(audioURL)
	gcsURI := fmt.Sprintf("gs://%s/%s", Bucket, base)
	log.Println("gcsURI:", gcsURI)

	resp, err := sendAudioOnGCS(gcsURI)
	if resp == nil {
		postTranscribeFailed(r)
		return
	}
	if err != nil {
		log.Fatalf("Failed to send .wav to Speech API: %v\n", err)
	}

	transcriptFile := strings.Replace(base, path.Ext(base), ".txt", 1)
	if err := uploadTranscript(resp.Results, transcriptFile); err != nil {
		log.Fatalf("Failed to upload transcript file: %v\n", err)
	}
	transcriptURI := fmt.Sprintf("https://storage.googleapis.com/%s/%s", Bucket, transcriptFile)

	postTranscript(r, transcriptURI)
}

var (
	db  *sql.DB
	err error
	tc  *twilio.Client
	sc  *slack.Client
)

func init() {
	// DB connetction
	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("Error opening database: %v", err)
	}

	// Twilio Client
	{
		id := os.Getenv("TWILIO_ACCOUNT_SID")
		tk := os.Getenv("TWILIO_AUTH_TOKEN")
		tc = twilio.NewClient(id, tk, nil)
	}

	// Slack Client
	{
		tk := os.Getenv("SLACK_API_TOKEN")
		sc = slack.New(tk)
	}
}

func main() {
	defer db.Close()
	log.Println("=== start ===")

	createTables()
	insertCalls()
	insertRecs()
	readDB()
}
