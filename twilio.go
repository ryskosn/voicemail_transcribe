package main

import (
	"context"
	"fmt"
	"log"
	"net/url"
	"time"

	twilio "github.com/saintpete/twilio-go"
)

func fetchCalls() twilio.CallPageIterator {
	now := time.Now()
	// from 1 day ago
	start := now.AddDate(0, 0, -1)
	return tc.Calls.GetCallsInRange(start, now, url.Values{})
}

func showCalls(iter twilio.CallPageIterator) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	for {
		page, err := iter.Next(ctx)
		if err == twilio.NoMoreResults {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		for i, call := range page.Calls {
			t := jst(call.DateCreated.Time)
			fmt.Printf("%d: %s (%s)\n", i, call.To, t)
		}
	}
}

func fetchRecordings() (*twilio.RecordingPage, error) {
	ctx := context.Background()
	return tc.Recordings.GetPage(ctx, url.Values{})
}

func showRecordings(page *twilio.RecordingPage) {
	for i, rec := range page.Recordings {
		t := jst(rec.DateCreated.Time)
		fmt.Printf("%d: %s (%s) %s\n", i, t, rec.URL(".wav"), rec.Duration.String())
	}
}
