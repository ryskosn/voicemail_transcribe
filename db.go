package main

import (
	"context"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
	twilio "github.com/saintpete/twilio-go"
)

func createTables() {
	q1 := `
    CREATE TABLE IF NOT EXISTS
    calls (
           sid          TEXT PRIMARY KEY
           , created_at TIMESTAMP
           , to_        TEXT
           , from_      TEXT
          )
    ;`
	if _, err := db.Exec(q1); err != nil {
		log.Fatalf("Error creating database table calls: %q", err)
	}
	q2 := `
    CREATE TABLE IF NOT EXISTS
    recs (
          sid            TEXT PRIMARY KEY
          , created_at   TIMESTAMP
          , call_sid     TEXT
          , wav_url      TEXT
          , mp3_url      TEXT
          , duration     TEXT
          , transcribed  BOOLEAN DEFAULT false
          , posted_slack BOOLEAN DEFAULT false
         )
    ;`
	if _, err := db.Exec(q2); err != nil {
		log.Fatalf("Error creating database table recs: %q", err)
	}
}

func insertCall(call *twilio.Call) {
	t := jst(call.DateCreated.Time)
	q := `
    INSERT INTO
        calls (sid, created_at, to_, from_)
    SELECT
        '%[1]s', '%[2]s', '%[3]s', '%[4]s'
    WHERE
        NOT EXISTS(
            SELECT
                'x'
            FROM
                calls
            WHERE
                sid = '%[1]s'
        )
    ;`
	q = fmt.Sprintf(
		q,
		call.Sid,
		t.Format(time.RFC3339),
		call.To,
		call.From,
	)
	if _, err := db.Exec(q); err != nil {
		log.Printf("failed to insert call: %v\n", err)
	}
}

func insertCalls() {
	iter := fetchCalls()
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	for {
		page, err := iter.Next(ctx)
		if err == twilio.NoMoreResults {
			break
		}
		if err != nil {
			log.Fatalf("Failed to iterate Twilio Calls page: %v\n", err)
		}
		for _, call := range page.Calls {
			insertCall(call)
		}
	}
}

func insertRec(rec *twilio.Recording) {
	t := jst(rec.DateCreated.Time)
	q := `
    INSERT INTO
        recs (sid, created_at, call_sid, wav_url, mp3_url, duration)
    SELECT
        '%[1]s', '%[2]s', '%[3]s', '%[4]s', '%[5]s', '%[6]s'
    WHERE
        NOT EXISTS(
            SELECT
                'x'
            FROM
                recs
            WHERE
                sid = '%[1]s'
        )
    ;`
	q = fmt.Sprintf(
		q,
		rec.Sid,
		t.Format(time.RFC3339),
		rec.CallSid,
		rec.URL(".wav"),
		rec.URL(".mp3"),
		rec.Duration.String(),
	)
	if _, err := db.Exec(q); err != nil {
		log.Printf("Failed to insert rec: %v\n", err)
	}
}

func insertRecs() {
	iter, err := fetchRecordings()
	if err != nil {
		log.Printf("Failed to fetch Recordings: %v\n", err)
	}
	for _, rec := range iter.Recordings {
		insertRec(rec)
	}
}

// Rec is used by transcribe service
type Rec struct {
	sid       string `db:"sid"`
	createdAt string `db:"created_at"`
	wavURL    string `db:"wav_url"`
	mp3URL    string `db:"mp3_url"`
	to        string `db:"to_"`
	duration  string `db:"duration"`
}

func updateStatus(r Rec) {
	q := `
    UPDATE
        recs
    SET
        transcribed = true
    WHERE
        sid = '%s'
    ;`
	q = fmt.Sprintf(q, r.sid)
	if _, err := db.Exec(q); err != nil {
		log.Printf("Failed to update rec status: %v\n", err)
	}
}

func readDB() {
	q := `
    SELECT
        recs.sid
        , recs.created_at
        , recs.wav_url
        , recs.mp3_url
        , calls.to_
        , recs.duration
    FROM
        recs
        JOIN calls ON recs.call_sid = calls.sid
    WHERE
        recs.transcribed = false
    ORDER BY recs.created_at ASC
    ;`

	rows, err := db.Query(q)
	if err != nil {
		log.Println(err)
	}
	defer rows.Close()

	for rows.Next() {
		var r Rec
		rows.Scan(
			&r.sid,
			&r.createdAt,
			&r.wavURL,
			&r.mp3URL,
			&r.to,
			&r.duration,
		)
		transcribe(r)
		updateStatus(r)
	}
}
