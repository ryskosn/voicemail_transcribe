package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"cloud.google.com/go/storage"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
)

// uploadTwilioWav uploads WAV file which is hosting on Twilio
// to Google Cloud Storage.
func uploadTwilioWav(fileURL string) error {
	resp, err := http.Get(fileURL)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("Status Code: %d", resp.StatusCode)
	}

	ctx := context.Background()
	name := filepath.Base(fileURL)
	_, objAttrs, err := upload(ctx, resp.Body, name, false)
	if err != nil {
		fmt.Println("Failed to upload file to GCS...")
		return err
	}
	log.Printf("Size: %d", objAttrs.Size)
	return nil
}

// uploadTranscript uploads transcription results text file.
func uploadTranscript(results []*speechpb.SpeechRecognitionResult, name string) error {
	var lines []string
	for _, result := range results {
		for _, alt := range result.Alternatives {
			lines = append(lines, alt.Transcript)
		}
	}
	r := strings.NewReader(strings.Join(lines, "\n"))

	ctx := context.Background()
	_, objAttrs, err := upload(ctx, r, name, true)
	if err != nil {
		fmt.Println("Failed to upload file")
		return err
	}
	log.Printf("Size: %d", objAttrs.Size)
	return nil
}

func upload(ctx context.Context, r io.Reader, name string, public bool) (*storage.ObjectHandle, *storage.ObjectAttrs, error) {

	json := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	jwtConfig, err := google.JWTConfigFromJSON([]byte(json), storage.ScopeFullControl)
	if err != nil {
		return nil, nil, errors.New("google.JWTConfigFromJSON :" + err.Error() + "\n" + json)
	}

	ts := jwtConfig.TokenSource(ctx)
	c, err := storage.NewClient(ctx, option.WithTokenSource(ts))
	// c, err := storage.NewClient(ctx)
	if err != nil {
		fmt.Println("Failed to create storage client")
		return nil, nil, err
	}

	bh := c.Bucket(Bucket)
	if _, err = bh.Attrs(ctx); err != nil {
		return nil, nil, err
	}

	obj := bh.Object(name)
	w := obj.NewWriter(ctx)
	if _, err := io.Copy(w, r); err != nil {
		return nil, nil, err
	}
	if err := w.Close(); err != nil {
		return nil, nil, err
	}

	if public {
		if err := obj.ACL().Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
			return nil, nil, err
		}
	}
	attrs, err := obj.Attrs(ctx)
	return obj, attrs, err
}

// ObjectURL returns object's public URL.
func ObjectURL(objAttrs *storage.ObjectAttrs) string {
	return fmt.Sprintf("https://storage.googleapis.com/%s/%s", objAttrs.Bucket, objAttrs.Name)
}
