'use strict';

/**
 * Background Cloud Function to be triggered by Cloud Storage.
 *
 * @param {object} event The Cloud Functions event.
 * @param {function} callback The callback function.
 */
exports.copyTranscriptGCS = (event, callback) => {
    const fs = require('fs');
    const path = require('path');

    const file = event.data;
    const ext = path.extname(file.name);
    // excludes extention
    const basename = path.basename(file.name, ext);
    const postfix = '_raw';

    if ((ext === '.txt') && !(basename.includes(postfix)) && (file.metageneration === '1')){
        // file.metageneration === '1' means the file is uploaded.
        const Storage = require('@google-cloud/storage');
        const destFilename = basename + postfix + '.txt';
        const projectId = 'voicemail-transcribe-193213';
        const storage = new Storage({
            projectId: projectId,
        });
        const bucketName = file.bucket;

        // copy file
        storage
            .bucket(bucketName)
            .file(file.name)
            .copy(storage.bucket(bucketName).file(destFilename))
            .then(() => {
                console.log(`${file.name} copied to ${destFilename}.`);
            })
            .catch(err => {
                console.error('ERROR:', err);
            });
    }
    callback();
};
