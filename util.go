package main

import (
	"log"
	"net/url"
	"os"
	"path"
	"strings"
	"time"
)

func mustGetEnv(envKey, defaultValue string) string {
	val := os.Getenv(envKey)
	if val == "" {
		val = defaultValue
	}
	if val == "" {
		log.Fatalf("%q should be set", envKey)
	}
	return val
}

// twilioWavURL returns .wav file url
func twilioWavURL(u *url.URL) string {
	base := path.Base(u.Path)
	ext := path.Ext(base)

	if ext == ".mp3" {
		dir := path.Dir(u.Path)
		base = strings.Replace(base, ext, ".wav", 1)
		u.Path = path.Join(dir, base)
	}
	return u.String()
}

// twilioMp3URL returns .mp3 file url
func twilioMp3URL(u *url.URL) string {
	base := path.Base(u.Path)
	ext := path.Ext(base)

	if ext == ".wav" {
		dir := path.Dir(u.Path)
		base = strings.Replace(base, ext, ".mp3", 1)
		u.Path = path.Join(dir, base)
	}
	return u.String()
}

func jst(t time.Time) time.Time {
	jst := time.FixedZone("Asia/Tokyo", 9*60*60)
	return t.In(jst)
}
