#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import io
import sys
import codecs
import datetime
import locale

def transcribe_file(speech_file):
    from google.cloud import speech
    from google.cloud.speech import enums
    from google.cloud.speech import types
    client = speech.SpeechClient()

    with io.open(speech_file, 'rb') as audio_file:
        content = audio_file.read()

    audio = types.RecognitionAudio(content=content)
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=8000,
        language_code='ja-JP'
    )

    operation = client.long_running_recognize(config, audio)

    print('Waiting for operation to complete...')
    response = operation.result(timeout=900)

    for result in response.results:
        print('Transcript: {}'.format(result.alternatives[0].transcript))
        print('Confidence: {}'.format(result.alternatives[0].confidence))


def transcribe_gcs(gcs_uri):
    from google.cloud import speech
    from google.cloud.speech import enums
    from google.cloud.speech import types
    client = speech.SpeechClient()

    audio = types.RecognitionAudio(uri=gcs_uri)
    config = types.RecognitionConfig(
        sample_rate_hertz=8000,
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        language_code='ja-JP')

    operation = client.long_running_recognize(config, audio)

    print('Waiting for operation to complete...')
    operationResult = operation.result()

    d = datetime.datetime.today()
    today = d.strftime("%Y%m%d-%H%M%S")
    fout = codecs.open('output{}.txt'.format(today), 'a', 'utf8')

    for result in operationResult.results:
      for alternative in result.alternatives:
          fout.write('{}\n'.format(alternative.transcript))
          # fout.write('{}\n'.format(alternative.confidence))
    fout.close()


if __name__ == '__main__':
    gcs_bucket = 'voicemail-193213'
    filename = 'REc1716c.wav'
    gcs_uri = "gs://{}/{}".format(gcs_bucket, filename)
    transcribe_gcs(gcs_uri)

    # parser = argparse.ArgumentParser(
    #     description=__doc__,
    #     formatter_class=argparse.RawDescriptionHelpFormatter)
    # parser.add_argument(
    #     'path', help='GCS path for audio file to be recognized')
    # args = parser.parse_args()
    # transcribe_gcs(args.path)
