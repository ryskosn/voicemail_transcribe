#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
from google.cloud import storage

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to bucket"""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {}'.format(
        source_file_name, destination_blob_name))


def upload_online_blob(bucket_name, source_file_uri, destination_blob_name):
    """Uploads a online file to bucket"""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    response = requests.get(source_file_uri)
    content_type = 'audio/wav'
    blob.upload_from_string(response.content, content_type)

    print('File {} uploaded to {}'.format(
        source_file_uri, destination_blob_name))



def main():
    bucket = 'voicemail-193213'
    # source = 'sample.txt'
    # destination = 'cloud_sample.txt'
    # upload_blob(bucket, source, destination)

    # uri = 'https://api.twilio.com/2010-04-01/Accounts/AC2592f5c820cf721d06e6b281206e1677/Recordings/REc1716c661ad8f1e3eaa4a970918a2c73.wav'
    uri = 'https://api.twilio.com/2010-04-01/Accounts/AC2592f5c820cf721d06e6b281206e1677/Recordings/REb3f5eda12cc3c6391e9fc00cb72a283d.wav'
    upload_online_blob(bucket, uri, 'REb3f5eda1.wav')

if __name__ == '__main__':
    main()
