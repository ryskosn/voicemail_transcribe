package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/nlopes/slack"
)

func run(api *slack.Client) int {
	rtm := api.NewRTM()
	go rtm.ManageConnection()

	for {
		select {
		case msg := <-rtm.IncomingEvents:
			switch ev := msg.Data.(type) {
			case *slack.HelloEvent:
				log.Print("Hello Event")

			case *slack.MessageEvent:
				log.Printf("Message: %v\n", ev)
				rtm.SendMessage(rtm.NewOutgoingMessage("Hello world!", ev.Channel))

			case *slack.InvalidAuthEvent:
				log.Print("Invalid credentials")
				return 1
			}
		}
	}
}

func getChannel(to string) string {
	var nums struct {
		Numbers []struct {
			Name      string `json:"name"`
			To        string `json:"to"`
			SlackChan string `json:"channel"`
		} `json:"numbers"`
		Default string `json:"default"`
	}
	j := os.Getenv("TWILIO_NUMBERS")
	json.Unmarshal([]byte(j), &nums)

	for _, num := range nums.Numbers {
		if to == num.To {
			return num.SlackChan
		}
	}
	return nums.Default
}

const (
	botName       = "postman"
	botIcon       = ":smiley_cat:"
	botIconFailed = ":crying_cat_face:"
)

type postParams struct {
	msg       string
	title     string
	titleLink string
	preText   string
	text      string
	channel   string
	icon      string
}

func post(p postParams) {
	params := slack.PostMessageParameters{}
	params.Username = botName
	params.IconEmoji = p.icon
	params.Markdown = true

	attachment := slack.Attachment{
		Title:     p.title,     // Twilio mp3 filename
		TitleLink: p.titleLink, // Twilio mp3 URL
		Pretext:   p.preText,   // transcript URL
		Text:      p.text,      // transcript text
	}
	params.Attachments = []slack.Attachment{attachment}

	chanID, timestamp, err := sc.PostMessage(p.channel, p.msg, params)
	if err != nil {
		log.Println("Failed to post message to Slack:", err)
		return
	}
	fmt.Printf("Message successfully sent to channel %s at %s\n", chanID, timestamp)
}

func prePost(r Rec) postParams {
	t, err := time.Parse(time.RFC3339, r.createdAt)
	if err != nil {
		log.Println("Failed to parse rec.createdAt:", err)
	}
	var p postParams
	p.msg = "*" + t.Format("1/2 15:04:05") + " - " + r.duration + "*"
	p.title = path.Base(r.mp3URL)
	p.titleLink = r.mp3URL
	p.channel = getChannel(r.to)
	return p
}

func postTranscript(r Rec, transcriptURI string) {
	resp, err := http.Get(transcriptURI)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("Status Code: %d", resp.StatusCode)
	}

	trscrpt, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Failed to ReadAll transcript:", err)
	}

	p := prePost(r)
	p.preText = transcriptURI
	p.text = string(trscrpt)
	p.icon = botIcon
	post(p)
}

func postTranscribeFailed(r Rec) {
	p := prePost(r)
	p.text = "Cloud Speech API returns nothing, voicemail seems to be empty..."
	p.icon = botIconFailed
	post(p)
}
