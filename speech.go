package main

import (
	"errors"
	"log"
	"os"

	speech "cloud.google.com/go/speech/apiv1"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"
)

// sendAudioOnGCS sends audio file on Cloud Storage
// to Cloud Speech API
func sendAudioOnGCS(gcsURI string) (*speechpb.LongRunningRecognizeResponse, error) {
	json := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	ctx := context.Background()

	jwtConfig, err := google.JWTConfigFromJSON([]byte(json), speech.DefaultAuthScopes()[0])
	if err != nil {
		return nil, errors.New("google.JWTConfigFromJSON :" + err.Error() + "\n" + json)
	}
	ts := jwtConfig.TokenSource(ctx)

	c, err := speech.NewClient(ctx, option.WithTokenSource(ts))
	// c, err := speech.NewClient(ctx)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	req := &speechpb.LongRunningRecognizeRequest{
		Config: &speechpb.RecognitionConfig{
			Encoding:        speechpb.RecognitionConfig_LINEAR16,
			SampleRateHertz: 8000,
			LanguageCode:    "ja-JP",
		},
		Audio: &speechpb.RecognitionAudio{
			AudioSource: &speechpb.RecognitionAudio_Uri{Uri: gcsURI},
		},
	}
	op, err := c.LongRunningRecognize(ctx, req)
	if err != nil {
		return nil, err
	}
	return op.Wait(ctx)
}
